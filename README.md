# shiny_docker

## Description
This project provides a dockerfile for a [Shiny Server](https://posit.co/products/open-source/shinyserver) based on https://gitlab.opencode.de/r-statistik/r_docker.

## Build
As per usual basic usage is `docker build -t <repo>` and `docker run <repo>`, usually the project will be used in as a base image though.

    FROM r-statistik/shiny_docker:<version>

The tag of the base image to be used can be set using `--build-arg BASE_TAG=<tag>`.

## Usage

The server will serve projects mounted at `/srv/shiny-server`. Any project below this folder will be accessible e.g.
`/srv/shiny-server/project1` is provided at `<host>/project1`.

The servers root `<host>/` is configured to provide live access to the logs produced by the server, see next chapter
regarding security.

## Security

The image provides two ways for accessing the logs produced by the shiny server, which are a simple directory access via
nginx and log.io.
Since these should clearly not be accessible without a login, the Dockerfile includes a mechanism to secure the provided
endpoints `<domain>/` and `<domain>/logs-raw/` respectively with a password, which can be set via build-arg `--build-arg PASSWORD_LOGS=<password>`.

Do not use the image without changing the default password `insecure!` for any other purpose than testing!

### Securing log access when using the prebuilt image

If the provided image is used instead of building a custom image with a custom password, the password can be overridden
by mounting custom versions of the files `/etc/nginx/.htpasswd` and ` /home/$user/.log.io/server.json`, the former being
a [basic authentication file](https://en.wikipedia.org/wiki/.htpasswd) as used by Apache HTTP Server, the other being the
log.io configuration file, where the password is configured (replace `<password>`).

    {
      "messageServer": {
        "port": 6689,
        "host": "127.0.0.1"
      },
      "httpServer": {
        "port": 6688,
        "host": "127.0.0.1"
      },
      "basicAuth": {
        "realm": "shiny",
        "users": {
          "audit": "<password>"
        }
      }
    }

## License
Eclipse Public License - v 2.0
