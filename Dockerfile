ARG BASE_TAG=1.0.6

FROM registry.opencode.de/r-statistik/r_docker:$BASE_TAG
LABEL org.opencontainers.image.authors="rene.pasold@jena.de"

EXPOSE 3838

# Configure in build as arg!
ARG PASSWORD_LOGS="insecure!"
ARG user=r

USER install
WORKDIR /home/install

RUN yay -Syyu --noconfirm \
 && yay -Scc --noconfirm

# Shiny Server
RUN yay -S --needed --noconfirm nginx \
 && git clone https://aur.archlinux.org/shiny-server.git \
 && cd shiny-server \
 && makepkg --verifysource \
 && sed -ri -e "s/(\s)shiny/\1$user/g" shiny-server.tmpfiles \
 && sed -ri -e "s/(\s|=)shiny/\1$user/g" shiny-server.service \
 && sed -ri -e "s/(\s|=)shiny/\1$user/g" shiny-server.sysusers \
 && makepkg -sri --needed --noconfirm --skipinteg \
 && yay -Scc --noconfirm

USER root
WORKDIR /root/

# Add password for logs
RUN printf "audit:$(openssl passwd -apr1 ${PASSWORD_LOGS})\n" >> /etc/nginx/.htpasswd \
 && gpasswd -a http $user \
 && mkdir /tmp/r \
 && chown -R $user:$user /tmp/r \
 && echo "options(cache = cachem::cache_disk(\"/tmp/r\"))" >> /home/$user/.Rprofile

# Remove installation user
RUN userdel install \
 && rm /etc/sudoers.d/install \
 && rm -rf /home/install

# log.io
RUN pacman -S --needed --noconfirm npm \
 && npm install -g log.io \
 && npm install -g log.io-file-input \
 && pacman -Scc --noconfirm

USER $user
WORKDIR /home/$user

RUN  echo -e "export PATH=/home/$user/.local/bin:\$PATH\n" >> /home/$user/.bash_profile

COPY resources/shiny-server.conf /etc/shiny-server/shiny-server.conf
COPY resources/nginx.conf /etc/nginx/nginx.conf
COPY resources/proxy.conf /etc/nginx/sites-enabled/shiny.conf

COPY --chown=$user:$user resources/logio-file.json /home/$user/.log.io/inputs/file.json
COPY --chown=$user:$user resources/logio-server.json /home/$user/.log.io/server.json
COPY --chown=$user:$user resources/r-startup.sh /home/$user/.local/bin/

USER root

RUN sed -i "s/<password>/${PASSWORD_LOGS/&/\\&}/g" /home/$user/.log.io/server.json

COPY resources/startup.sh /startup.sh

CMD [ "/startup.sh" ]
