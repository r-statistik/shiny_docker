#!/usr/bin/sh
for line in $( cat /etc/environment | grep -v "^#" ) ; do export ${line} > /dev/null; done

export NODE_ENV="production"
export NODE_OPTIONS="--max_old_space_size=1024"

log.io-server &
(sleep 5 && log.io-file-input) &
shiny-server
